adatok = []
with open("kerites.txt","r") as f:
    for i in f:
        i = i.strip().split()
        adatok.append({"oldal":int(i[0]),"méret":int(i[1]), "szín":i[2], "házszám":0})
print(f"2.feladat\nAz eladott telkek száma: {len(adatok)} \n")


páros_oldal = 0
páratlan_oldal = 0
index = 0
for i in adatok: 
    if i["oldal"] == 0:
        
        páros_oldal += 1
        # print(páros_oldal*2, "p")
        adatok[index]["házszám"] = páros_oldal*2
        index += 1
    elif i["oldal"] == 1:
        páratlan_oldal += 1
        # print(páratlan_oldal+(páratlan_oldal-1), "pl")
        adatok[index]["házszám"] = páratlan_oldal+(páratlan_oldal-1)
        index += 1
# print(páros_oldal, páratlan_oldal)
if adatok[-1]["oldal"] == 0:
    print(f"3.feladat\nA páros oldalon adták el az utolsó telket.\nAz utolsó telek házszáma: {páros_oldal*2}\n")
else:
    
    print(f"3.feladat\nA páratlan oldalon adták el az utolsó telket.\nAz utolsó telek házszáma: {páratlan_oldal+(páratlan_oldal-1)}\n")


házszám1 = 0
szín1 = ""

current = 0
színek = []
for i in adatok:
    if i["szín"] not in színek:
        
        színek.append(i["szín"])

színek.remove("#")
színek.remove(":")

for i in adatok:
    
    if current == 0 and i["oldal"] == 1 and i["szín"] in színek:
        házszám1 = i["házszám"]
        szín1 = i["szín"]
        current = 1
        
    elif current == 1 and i["oldal"] == 1 and i["szín"] in színek:
        
        if i["szín"] == szín1:
            
            break
        current = 0
print(f"4.feladat\nA szomszédossal megyezik a kerítés színe: {házszám1}\n")


#5.feladat
print("5.feladat")
# házszám_in = int(input("Adjon meg egy házszámot! "))
házszám_in = 83
szín5_in = ""
szín5_1 = ""
szín5_2 = ""
for i in adatok:
    if i["házszám"] == házszám_in:
        szín5_in = i["szín"]
    if i["házszám"] == házszám_in-2:
        szín5_1 = i["szín"]
    if i["házszám"] == házszám_in+2:
        szín5_2 = i["szín"] 
színek5 = színek
színek5.remove(szín5_1)
színek5.remove(szín5_2)
színek5.remove(szín5_in)
print(f"A kerítés színe / állapota: {szín5_in}\nEgy lehetséges festési szín: {színek5[0]}")

#6.feladat

sor1 = ""
sor2 = ""
for i in adatok:
    if i["oldal"] == 1:
        sor1 += i["méret"]*i["szín"]  
        sor2 += str(i["házszám"]) + (i["méret"]-1)*" "
utcakep = open("utcakep.txt","w")
utcakep.write(f"{sor1}\n{sor2}")

