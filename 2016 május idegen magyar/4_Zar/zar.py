with open("ajto.txt","r") as ff:
    adatok = [sor.strip() for sor in ff ]

print("2. feladat")
kódIn = input("Adja meg, mi nyitja a zárat! ") 
# kódIn = "239451"
print("3. feladat")
kódLista = " ".join([str(sor+1) for sor, kód in enumerate(adatok) if kód == kódIn])
print("A nyitó kódszámok sorai:",kódLista)

volt = False
for index, i in enumerate(adatok):
    for num in i:
        if i.count(num) > 1:
            print(f"Az első ismétlődő karaktert tartalmazó probálkozás sorszáma: {index+1}")
            volt = True
            break
    if volt:
        break
if volt == False:
    print("Nem volt ismétlődő számjegy")

import random
randomkód = ""
while len(randomkód) < len(kódIn):
    randomSzám = random.randint(0,9)
    if str(randomSzám) not in randomkód:
        randomkód += str(randomSzám)
print("5. feladat")
print(f"Egy {len(kódIn)} hosszú kódszám: {randomkód}")

def nyit(jo, proba):
    egyezik = True if len(jo) == len(proba) else False
    if egyezik:
        elteres= ord(jo[1])-ord(proba[1])
        for i in range(2, len(jo)):
            if (elteres - (ord(jo[i])-ord(proba[i]))) % 10 > 0:
                egyezik = False
    return egyezik

with open("siker.txt","w", encoding="UTF-8") as kf:
    for i in adatok:
        if len(i) == len(kódIn):
            if nyit(kódIn, i):
                kf.write(f"{i} sikeres\n")
            else:
                kf.write(f"{i} hibás kódszám\n")

        else:
            kf.write(f"{i} hibás hossz\n")