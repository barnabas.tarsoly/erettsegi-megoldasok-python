with open("foglaltsag.txt","r") as ff:
    foglaltság = []
    for sor in ff:
        foglaltság.append(sor.strip())

with open("kategoria.txt","r") as ff2:
    ketegória = []
    for sor in ff2:
        ketegória.append(sor.strip())

print("2. feladat")
sor = int(input("Sor száma: "))
szék = int(input("Szék száma: "))

if foglaltság[sor-1][szék-1] == "o":
    print("A szék még szabad.")
else:
    print("A szék már foglalt.")

print("\n3. feladat")
összes = 15*20 #szék
eladott = 0
for sor in foglaltság:
    eladott += sor.count('x')
print(f"Az előadásra eddig {eladott} jegyet adtak el, ez a nézőtár {round(eladott*100/összes)}%-a")

kategóriák = [0, 0, 0, 0, 0]
árak = (5000, 4000, 3000, 2000, 1500)
bevétel = 0
for index, sor in enumerate(ketegória):
    
    for index2, sor2 in enumerate(sor):
        if foglaltság[index][index2] == "x":
            bevétel += árak[int(sor2)-1]
            kategóriák[int(sor2)-1] += 1
            
print("\n4. feladat")
print(f"A legtöbb jegyet a(z) {kategóriák.index(max(kategóriák))+1}. árkategóriában értékesítették.")

print("\n5. feladat")
print(f"A pillanatnyi bevétel: {bevétel} Ft")

egyesÜres = 0
for sor in foglaltság:
    

    for index, szék in enumerate(sor):
        if index == 0:
            if szék == "o" and sor[index+1] == "x":
                egyesÜres += 1
        elif index == len(sor)-1:
            if szék == "o" and sor[index-1] == "x":
                egyesÜres += 1
        elif sor[index-1] == "x" and szék == "o" and sor[index+1] == "x":
            egyesÜres += 1
print("\n6. feladat")
print(f"{egyesÜres} darab egyedülálló üres hely van a nézőtéren.")

with open("szabad.txt","w",encoding="UTF-8") as kf:

    for index, sor in enumerate(ketegória):
        szöveg = ""
        for index2, sor2 in enumerate(sor):
            if foglaltság[index][index2] == "x":
                szöveg += "x"
            else:
                szöveg += f"{sor2}"
        kf.write(f"{szöveg}\n")
