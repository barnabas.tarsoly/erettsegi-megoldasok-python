with open("tavok.txt","r") as ff:
    adatok = []
    for sor in ff:
        sor = list(map(int, sor.strip().split()))
        nap = sor[0]
        fuvar = sor[1]
        km = sor[2]
        adatok.append({"nap":nap,"fuvar":fuvar,"km":km})


napok = list(set([x["nap"] for x in adatok]))
napok.sort()
print("2. feladat:")
for x in adatok:

    if x["nap"] == napok[0] and x["fuvar"] == 1:
       
        print(f"A hét első útja {x['km']} km volt.")
    
utolsó_fuvar = [x["km"] for x in adatok if x["nap"] == napok[-1]]
print("3. feladat:")
print(f"Az utlsó út {utolsó_fuvar[-1]} km volt.")
print("4. feladat:")
for x in range(7):
    if x+1 not in napok:
        print(f"A futár a {x+1}. napon nem dolgozott")

napi_adatok = []
fuvarok = []
for i in range(7):
    darab = len([x for x in adatok if x["nap"] == i+1])
    napi_adatok.append({"nap":i+1,"darab":darab})
    fuvarok.append(darab)
print("5. feladat:")
for x in napi_adatok:
    if x["darab"] == max(fuvarok):
        print(f'A {x["nap"]}. napon volt a legtöbb fuvarja.')

# összegzés = [{"nap":x+1, "km":0} for x in range(7)]
print("6. feladat")
for i in range(7):
    
    km = sum([x["km"] for x in adatok if x["nap"] == i+1])
    print(f"{i+1}. nap: {km} km")


print("7. feladat:")
km_in = int(input("Km (max 30): "))

def ár(km):
    if km >= 1 and km <= 2:
        return 500
    elif km >= 3 and km <= 5:
        return 700
    elif km >= 6 and km <= 10:
        return 900
    elif km >= 11 and km <= 20:
        return 1400
    elif km >= 21 and km <= 30:
        return 2000

print(f"A {km_in} km-ért {ár(km_in)} Ft jár.")
with open("dijaza.txt","w", encoding="utf-8") as kf:

    összeg = 0
    for i in range(7):
        napi_furvarok = [x for x in adatok if x["nap"] == i+1]
        temp_km = []
        for y in range(len(napi_furvarok)):
            temp_km.append(napi_furvarok[y]["km"])
        # print(temp_km)
        for index, x in enumerate(temp_km):
            kf.write(f"{i+1}. nap {index+1}. út: {ár(x)}\n")
            összeg += ár(x)
        # összeg += sum(temp_km)
    print("9. feladat:")
    print("A heti munkájáért:", összeg, "Ft-ot kap.")