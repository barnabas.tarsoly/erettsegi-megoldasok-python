import datetime
with open("fogado.txt", "r") as ff:
    adatok = []
    for sor in ff:
        sor = sor.strip().split()
        név = sor[:2]
        foglaltÓra = sor[2]
        rögzítés = sor[3]
        rögzítésSec = datetime.datetime.strptime(rögzítés, "%Y.%m.%d-%H:%M")
        adatok.append({"név":" ".join(név),"foglaltÓra":foglaltÓra,"rögzítés":rögzítés, "rögzítésSec":rögzítésSec})

print("2. feladat")
print(f"Foglalások száma: {len(adatok)}")
print("\n3. feladat")
névIn = input("Adjom meg egy nevet: ")
nincs = True
darab = 0
for adat in adatok:
    if adat["név"] == névIn:
        darab += 1
        nincs = False
if nincs:
    print("A megadott néven nincs időpontfoglalás.")
else:
    print(f"{névIn} néven {darab} időpontfoglalás vna.")
print("\n 4. feladat")
időpontIn = input("Adjom meg egy érvényes időpontot (pl. 14:10): ")
nevek = []
for adat in adatok:
    if adat["foglaltÓra"] == időpontIn:
        if adat["név"] not in nevek:
            nevek.append(adat["név"])
nevek.sort()
with open(f"{''.join(időpontIn.split(':'))}.txt", 'w', encoding="utf-8") as kf:
    for név in nevek:
        print(név)
        kf.write(f"{név}\n")
print("\n5. feladat")
legkisebb = [x["rögzítésSec"] for x in adatok]
for adat in adatok:
    if adat["rögzítésSec"] == min(legkisebb):
        print(f"Tanár neve: {adat['név']}\nFoglalt időpont: {adat['foglaltÓra']}\nFogaláls ideje: {adat['rögzítés']}")

print("\n6. feladat")
órák = ["16:00","16:10","16:20","16:30","16:40","16:50","17:00","17:10","17:20", "17:30","17:40","17:50"]
órákCopy = órák.copy()
for adat in adatok:
    if adat["név"] == "Barna Eszter":
        órák.remove(adat["foglaltÓra"])
for óra in órák:
    print(óra)
utolsó = "16:00"
for óra in órákCopy:
    if óra not in órák:
        utolsó = óra
index = órákCopy.index(utolsó)
if index == len(órákCopy)-1:
    távozhat = "18:00"
else:
    távozhat = órákCopy[index+1]
print(f"Barna Eszter legkorábban távozhat: {távozhat}")

