with open("penztar.txt","r",encoding="UTF-8") as ff:   
    eddigVásárolt = []
    adatok = []
    vásárlóSzáma = 1
    fizetett = 0
    for sor in ff:
        sor = sor.strip()
        if sor == "F":
            áruk = []
            for i in eddigVásárolt:
                if i not in áruk:
                    áruk.append(i)
            for i in áruk:
                darab = eddigVásárolt.count(i)
                if darab == 1:
                    fizetett += 500
                elif darab == 2:
                    fizetett += 500+450
                elif darab == 3:
                    fizetett += 500+450+400
                else:
                    fizetett += 500+450+400+(400*(darab-3))
            adatok.append({"id":vásárlóSzáma,"áru":eddigVásárolt,"fizetett":fizetett,"áruk":áruk})
            vásárlóSzáma += 1
            eddigVásárolt = []
            áruk = []
            fizetett = 0
        else:
            eddigVásárolt.append(sor)

print("2. feladat")
print(f"A fizetett esetek száma: {len(adatok)}")

print("\n3. feladat")
print(f"Az első vásárló {len(adatok[0]['áru'])} darab árucikket vásárolt.")

print("\n4. feladat")
sorszám = int(input("Adja meg egy vásárlás sorszámát! "))-1
árucikk = input("Adja meg egy árucikk nevét! ")
darabszám = int(input("Adja meg a vásárolt darabszámot! "))

print("\n5. feladat")
elsőnek = 0
első = True
utoljára = 0
darabszorVásároltak = 0 
for adat in adatok:
    if árucikk in adat["áru"] and első == True:
        elsőnek = adat["id"]
        első = False
    elif árucikk in adat["áru"] and első == False:
        utoljára = adat["id"]
    if árucikk in adat["áru"]:
        darabszorVásároltak += 1

print(f"Az első vásárlás sorszáma: {elsőnek}\nAz utolsó vásárlás sorszáma: {utoljára}\n{darabszorVásároltak} vásárlás során vettek belőle.")

print("\n6. feladat")

def ertek(darab):
    fizetett = 0
    if darab == 1:
        fizetett += 500
    elif darab == 2:
        fizetett += 500+450
    elif darab == 3:
        fizetett += 500+450+400
    else:
        fizetett += 500+450+400+(400*(darab-3))
    return fizetett
print(f"{darabszám} darab vételekor fizetendő: {ertek(darabszám)}")

print("\n7. feladat")
for i in adatok[sorszám]["áruk"]:
    print(f'{adatok[sorszám]["áru"].count(i)} {i}')

with open("osszeg.txt","w",encoding="UTF-8") as kf:
    for adat in adatok:
        kf.write(f"{adat['id']}:{adat['fizetett']}\n")