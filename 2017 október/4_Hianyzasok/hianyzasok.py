with open("naplo.txt","r") as ff:
    adatok = []
    igazolt = 0
    igazolatlan = 0
    for sor in ff:
        sor = sor.strip()
        if sor.startswith("#"):
            dátum = sor.split()[1:]
        else:
            sor = sor.split()
            név = sor[0:2]
            jelenlét = sor[2]
            igazolt += jelenlét.count("X")
            igazolatlan += jelenlét.count("I")
            adatok.append({"név":" ".join(név),"jelenlét":jelenlét,"dátum":dátum})
print("2. feladat")
print(f"A naplóban {len(adatok)} bejegyzés van.")
print("3. fealdat")
print(f"Az  igazolt hiányzások száma {igazolt}, az igazolatlanoké {igazolatlan} óra.")

def hetnapja(honap, nap):
    napnev = ["vasarnap", "hetfo", "kedd", "szerda", "csutortok", "pentek", "szombat"]
    napszam = [0,31,59,90,120,151,181,212,243,273,304,335]
    napsorszam = (napszam[honap-1]+nap) % 7
    hetnapja = napnev[napsorszam]
    return hetnapja

print("5. feladat")
hónapIn = int(input("A hónap sorszáma= "))
napIn = int(input("A nap sorszáma= "))
print(f"Azon a napon {hetnapja(hónapIn, napIn)} volt.")
print("6. feladat")
összesHiányzás = 0
napIn2 = input("A nap neve= ")
óraIn = int(input("Az óra sorszáma= "))
for adat in adatok:
    if hetnapja(int(adat["dátum"][0]),int(adat["dátum"][1])) == napIn2 and adat["jelenlét"][óraIn-1] == "X" or adat["jelenlét"][óraIn-1] == "I":
        összesHiányzás += 1
print(f"Ekkor összesen {összesHiányzás} óra hiányzás történt.")
print("7. feladat")
nevek = list(dict.fromkeys([név["név"] for név in adatok]))
hianyzasok = [0 for x in range(len(nevek))]
for adat in adatok:
    index = nevek.index(adat["név"])
    hianyzas = adat["jelenlét"].count("X") + adat["jelenlét"].count("I")
    hianyzasok[index] += hianyzas
maxHianyzas = max(hianyzasok)
hiányzók =[]
for i, x in enumerate(hianyzasok):
    if x == maxHianyzas:
        hiányzók.append(nevek[i])
print(f"A legtöbbet hiányzó tanulók: {' '.join(hiányzók)}")
    