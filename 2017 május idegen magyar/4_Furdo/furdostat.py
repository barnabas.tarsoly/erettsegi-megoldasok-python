with open("furdoadat.txt","r") as ff:
    adatok = []
    for sor in ff:
        sor = sor.strip().split()
        adatok.append({
            "id":int(sor[0]),
            "részleg":int(sor[1]),
            "ki":bool(int(sor[2])),
            "h":int(sor[3]),
            "m":int(sor[4]),
            "s":int(sor[5]),
        })

print("2. feladat")
első = {}
utolsó = {}

for x in adatok:
    if x["ki"]: 
        első = x
        break
for x in adatok:
    if x["ki"]: utolsó = x

print(f"Az első vendég {első['h']}:{első['m']}:{első['s']}-kor lépett ki az öltözőből.")
print(f"Az első vendég {utolsó['h']}:{utolsó['m']}:{utolsó['s']}-kor lépett ki az öltözőből.\n")


vendégek = {x["id"]:[] for x in adatok}

for x in adatok:
    
    vendégek[x["id"]].append(x) 
csak_1 = 0
for x in vendégek:
    if len(vendégek[x]) == 4:
        csak_1 += 1
print("3. feladat")
print(f"A fürdőben {csak_1} vendég járt csak egy részlegen.\n")

print("4. feladat")
eltöltött_idő = {x["id"]:0 for x in adatok}

for x in vendégek:
    
    eltöltött_idő[x] = (vendégek[x][-1]["h"]*60*60 + vendégek[x][-1]["m"]*60 + vendégek[x][-1]["s"])-(vendégek[x][0]["h"]*60*60 + vendégek[x][0]["m"]*60 + vendégek[x][0]["s"])


print("A legtöbb időt eltöltő vendég:")
vendég4 = max(eltöltött_idő, key=lambda x: eltöltött_idő[x]) 
eltöltött = eltöltött_idő[vendég4]
óra = eltöltött // (60*60)
perc = eltöltött % (60*60) // 60
mp = eltöltött % (60*60) % 60
# print(óra, perc, mp)
print(f"{vendég4}. vendég {óra}:{perc}:{mp}")

print("\n5. feladat")
reggel = 0
napközben = 0
este = 0

for x in vendégek:
    
    if vendégek[x][0]["h"] >= 6 and vendégek[x][0]["h"] < 9:
        reggel += 1
    elif  vendégek[x][0]["h"] >= 9 and vendégek[x][0]["h"] < 16:
        napközben += 1
    elif  vendégek[x][0]["h"] >= 16 and vendégek[x][0]["h"] < 20:
        este += 1

print(f"6-9 óra közöt {reggel} vendég")
print(f"9-16 óra közöt {napközben} vendég")
print(f"16-20 óra közöt {este} vendég\n")

szauna_idő = {x["id"]:0 for x in adatok}
for x in vendégek:
    for index, y in enumerate(vendégek[x]):
        if y["részleg"] == 2:
            if not y["ki"]:
                szauna_idő[x] += (vendégek[x][index+1]["h"]*60*60 + vendégek[x][index+1]["m"]*60 + vendégek[x][index+1]["s"])-(y["h"]*60*60 + y["m"]*60 + y["s"])

szauna_idő = {x:szauna_idő[x] for x in szauna_idő if szauna_idő[x] >0}
# print(len(szauna_idő))
with open("szauna.txt","w") as kf:
    for x in szauna_idő:
        óra = szauna_idő[x] // (60*60)
        perc = szauna_idő[x] % (60*60) // 60
        mp = szauna_idő[x] % (60*60) % 60
        kf.write(f"{x} {óra}:{perc}:{mp}\n")

print("7. feladat")
részleg = {x:0 for x in range(1,5)}
for x in vendégek:
    egyéni = {x:False for x in range(1,5)}
    for y in vendégek[x]:
        if y["részleg"] == 1:
            egyéni[1] = True

        elif y["részleg"] == 2:
            egyéni[2] = True

        elif y["részleg"] == 3:
            egyéni[3] = True

        elif y["részleg"] == 4:
            egyéni[4] = True

    for i in egyéni:
        if egyéni[i]:
            részleg[i] += 1
print(f"Uszoda: {részleg[1]}")
print(f"Szauna: {részleg[2]}")
print(f"Gyógyvizes medencék: {részleg[3]}")
print(f"Strand: {részleg[4]}")