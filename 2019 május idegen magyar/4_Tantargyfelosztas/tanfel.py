with open('beosztas.txt',"r") as ff:
    sorSzám = 1
    adatok = []
    for sor in ff:
        sor = sor.strip()
        if sorSzám == 1:
            név = sor
            sorSzám += 1
        elif sorSzám == 2:
            tárgy = sor
            sorSzám += 1
        elif sorSzám == 3:
            teljesOsztály = sor
            sor = sor.split(".")
            évfolyam = sor[0]
            osztály = sor[1]
            sorSzám += 1
        elif sorSzám == 4:
            adatok.append({"név":név,"tárgy":tárgy,"évfolyam":évfolyam, "osztály":osztály, "óraszám":int(sor),"teljesO": teljesOsztály})
            sorSzám = 1
print("2. feladat")
print(f"A fájlban {len(adatok)} bejegyzés van.")

print("3. feladat")
print(f"Az iskolában a heti összóraszám: {sum([x['óraszám'] for x in adatok])}")
print("4. feladat")
tanárIn = input("Egy tanár neve= ")
print(f'A tanár heti óraszáma: {sum([x["óraszám"] for x in adatok if x["név"] == tanárIn])}')

with open("of.txt","w", encoding="utf-8") as kf:
    for adat in adatok:
        if adat["tárgy"] == "osztalyfonoki":
            kf.write(f"{adat['évfolyam']}.{adat['osztály']} - {adat['név']}\n")
print("6. feladat")
osztályIn = input("Osztály= ")
tárgyIn = input("Tantárgy= ")
tantárgyDarab = 0
for adat in adatok:
    if adat["teljesO"] == osztályIn and adat["tárgy"] == tárgyIn:
        tantárgyDarab += 1
if tantárgyDarab == 1:
    print("Osztályszinten tanulják.")
else:
    print("Csoportbontásban tanulják.")

print("7. feladat")
print(f"Az iskolában {len(list(dict.fromkeys([x['név'] for x in adatok])))} tanár tanít.")