with open("valaszok.txt","r", encoding="UTF-8") as ff:
    elsőSor = True
    adatok = []
    for sor in ff:
        if elsőSor == True:
            sor = sor.strip()
            helyesVálasz = sor
            elsőSor = False
        else:
            sor = sor.strip().split()
            azonosító = sor[0]
            válasz = sor[1]
            adatok.append({"id":azonosító,"válasz":válasz})
print("1. feladat: Az adatok beolvasása")
print(f"2. feladat: A vetélkedőn {len(adatok)} versenyző indult.")

azonosítóInput = input("3. feladat: A versenyző azonosítója = ")

for versenyző in adatok:
    if versenyző["id"] == azonosítóInput:
        versenyzőVálasza = versenyző['válasz']
        print(f"{versenyzőVálasza}\t(a versenyző válasza)")

szöveg4 = ""
for i in range(len(helyesVálasz)):
    if helyesVálasz[i] == versenyzőVálasza[i]:
        szöveg4 = szöveg4 + "+"
    else:
        szöveg4 = szöveg4 + " "
print(f"4. feladat:\n{helyesVálasz}\t(a helyes megoldás)\n{szöveg4}\t(a versenyző helyes válaszai)")

sorszám = int(input("5. feladat: A feladat sorszáma = "))
darab = 0
for versenyző in adatok:
    if versenyző["válasz"][sorszám-1] == helyesVálasz[sorszám-1]:
        darab += 1
print(f"A feladatra {darab} fő, a verseznyzők {round((darab/len(adatok))*100,2)}%-a adott helyes választ.")

with open("pontok.txt","w") as kf:
    pontokInduló = []
    pontok = []
    print("6. feladat: A versenyzők pontszámának meghatározása.")
    for versenyző in adatok:
        pont = 0
        for i in range(len(helyesVálasz)):
            if versenyző["válasz"][i] == helyesVálasz[i]:
                feladat = i+1
                if feladat >= 1 and feladat <= 5:
                    pont += 3
                elif feladat >= 6 and feladat <= 10:
                    pont += 4
                elif feladat >= 11 and feladat <= 13:
                    pont += 5
                elif feladat == 14:
                    pont += 16  
        pontokInduló.append({"id":versenyző["id"], "pont":pont})
        pontok.append(pont)
        kf.write(f"{versenyző['id']} {pont}pont\n")

print("7. feladat: A verseny legjobbjai:") 

legnagyobbak = []
legnagyobb = 0
pontok.sort()
for pont in pontok:
    if pont not in legnagyobbak:
        legnagyobbak.append(pont)
legnagyobbak.sort(reverse=True)

elsőHárom = legnagyobbak[0:3]
helyezett = 1
for i in elsőHárom:
    for versenyző in pontokInduló:
        if i == versenyző["pont"]:
            print(f"{helyezett}. díj ({i} pont): {versenyző['id']}")
    helyezett += 1