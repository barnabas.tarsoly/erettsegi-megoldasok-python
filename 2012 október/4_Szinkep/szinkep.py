with open("kep.txt","r") as ff:
    adatok = []
    for sor in ff:
        adatok.append(tuple(map(int,sor.strip().split())))
print("2.feladat")
rgb = input("RGB kód (formátum: r g b): ")
rgb = tuple(map(int,rgb.split()))
if rgb in adatok:
    print("Benne van a képben.")   
else:
    print("Nincs benne a képben.")
soros_adatok = []
darab1 = 0
darab2 = 50
for x in range(50):
    soros_adatok.append(adatok[darab1:darab2])
    darab2 += 50
    darab1 += 50
sor, pixel = 35, 8
szín = soros_adatok[sor-1][pixel-1]
szerepel_sor = soros_adatok[sor-1].count(szín)
szerepel_oszlop = 0
for i in soros_adatok:
    if i[pixel-1] == szín:
        szerepel_oszlop += 1
print(f"Sorban: {szerepel_sor} Oszlopban: {szerepel_oszlop}")
red = adatok.count((255,0,0))
green = adatok.count((0,255,0))
blue = adatok.count((0,0,255))
if red > green and red > blue:
    print("A vörös többször szerepel")
elif green > red and green > blue:
    print("A zöld többször szerepel")
elif blue > red and blue > green:
    print("A kék többször szerepel")
vízszintes = [] 
for x in range(50):
    vízszintes.append((0,0,0))
soros_adatok[0] = vízszintes
soros_adatok[1] = vízszintes
soros_adatok[2] = vízszintes
for x, i in enumerate(soros_adatok):
    soros_adatok[x][0] = (0,0,0)
    soros_adatok[x][1] = (0,0,0)
    soros_adatok[x][2] = (0,0,0)
    soros_adatok[x][-1] = (0,0,0)
    soros_adatok[x][-2] = (0,0,0)
    soros_adatok[x][-3] = (0,0,0)
soros_adatok[-1] = vízszintes
soros_adatok[-2] = vízszintes
soros_adatok[-3] = vízszintes
with open("keretes.txt","w",encoding="utf-8") as kf:
    for x in soros_adatok:
        for y in x:
            kf.write(f"{str(y[0])} {str(y[1])} {str(y[2])}\n")
sárga_kód = (255,255,0)
Van_1 = False
első_koordináta = []
utolsó_koordináta = []
for index, sor in enumerate(soros_adatok):
    for index2, oszlop in enumerate(sor):
        if oszlop == sárga_kód and Van_1 == False:
            Van_1 = True
            első_koordináta = [index+1, index2+1]
        if oszlop == sárga_kód:
            utolsó_koordináta = [index+1, index2+1]
print(f"Kezd: {első_koordináta[0]}, {első_koordináta[1]}\nVége: {utolsó_koordináta[0]}, {utolsó_koordináta[1]}\nKéppontok száma: {adatok.count(sárga_kód)}")