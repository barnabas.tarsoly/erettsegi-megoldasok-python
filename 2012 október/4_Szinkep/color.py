from PIL import Image
import random
input_name = "keretes.txt"
output_name = "kep.jpg"

with open(input_name,"r") as ff:
	adatok = []
	for sor in ff:
		adatok.append(tuple(map(int,sor.strip().split())))



soros_adatok = []
darab1 = 0
darab2 = 50
for x in range(50):
	soros_adatok.append(adatok[darab1:darab2])
	darab2 += 50
	darab1 += 50
	
	
def drawImage():
	testImage = Image.new("RGB", (50,50), (255,255,255))
	pixel = testImage.load()
	for index1, x in enumerate(soros_adatok):
		for index2, y in enumerate(x):
			red = y[0]
			green = y[1]
			blue = y[2]
			
			pixel[index1,index2]=(red,blue,green)
	return testImage
def main():
	finalImage = drawImage()
	finalImage.save(output_name)
if __name__ == "__main__":
	main()