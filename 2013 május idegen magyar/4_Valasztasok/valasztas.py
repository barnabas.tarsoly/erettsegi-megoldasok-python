with open("szavazatok.txt","r") as ff:
    adatok = []
    for sor in ff:
        sor = sor.strip().split()
        adatok.append({"kerület": int(sor[0]), "szavazat":int(sor[1]), "név":sor[2]+" "+sor[3], "párt":sor[4]})
lakosok = 12345
print(f"\nA helyhatósági választáson {len(adatok)} képviselőjelölt indult.")

jelöltnév = input("\nÍrja be a jelölt vezeték és utónevét szókezzel elválasztva! ")
nincsNév = True
for jelölt in adatok:
    if jelölt["név"] == jelöltnév:
        nincsNév = False
        print(f"{jelöltnév} nevű képviselő {jelölt['szavazat']} szavazatot kapott.")
if nincsNév:
    print("Ilyen nevű képviselőjelölt nem szerepel a nyilvántartásban!")
    
résztvett = 0
for jelölt in adatok:
    résztvett += jelölt["szavazat"]

print(f"\nA választáson {résztvett} állampolgár, a jogosultak {round((résztvett*100)/lakosok,2)}%-a vett részt.")    

def pártnév(pártnév):
    if pártnév == "GYEP":
        return "Gyümölcsevők Pártja"
    elif pártnév == "HEP":
        return "Húsevők Pártja"
    elif pártnév == "TISZ":
        return "Tejivók SZövetsége"
    elif pártnév == "ZEP":
        return "Zöldségevők Pártja"
    else: 
        return "Független jelöltek"

pártok = dict.fromkeys([x["párt"] for x in adatok], 0)

for szavazatok in adatok:
    pártok[szavazatok["párt"]] += szavazatok["szavazat"]


for párt in list(pártok):
    print(f"{pártnév(párt)}={round((pártok[párt]*100)/lakosok,2)}%")
print("\n")
legnagyobb = [x["szavazat"] for x in adatok]
legnagyobb.sort(reverse=True)
for jelölt in adatok:
    if jelölt["szavazat"] == legnagyobb[0]:
        if jelölt["párt"] == "-":
            print(f"{jelölt['párt']}\t független")
        else:
            print(f"{jelölt['név']}\t {jelölt['párt']}")
kerületek = []
for i in range(8):
    kerületek.append({"kerület":i+1, "nyertes": "", "szavazat":0, "párt":""})

for index, kerület in enumerate(kerületek):
    for jelölt in adatok:
       
        if jelölt["kerület"] == kerület["kerület"]:
            if jelölt["szavazat"] >= kerület["szavazat"]:
                kerületek[index]["szavazat"] = jelölt["szavazat"]
                kerületek[index]["nyertes"] = jelölt["név"]
                if jelölt["párt"] == "-":
                    kerületek[index]["párt"] = "független"
                else:
                    kerületek[index]["párt"] = jelölt["párt"]
with open("kepviselok.txt","w", encoding="UTF-8") as kf:
    for kerület in kerületek:
        kf.write(f"{kerület['kerület']}\t{kerület['nyertes']}\t{kerület['párt']}\n")