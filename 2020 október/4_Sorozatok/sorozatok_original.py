with open("lista.txt","r") as ff:
    adatok = []
    index = 0
    for sor in ff:
        sor = sor.strip()
        if index == 0:
            dátum = sor
            index += 1
        elif index == 1:
            cím = sor
            index += 1
        elif index == 2:
            rész = sor
            index += 1
        elif index == 3:
            hossz = int(sor)
            index += 1
        elif index == 4:
            megtekintett = bool(int(sor))
            index += 1
        if index == 5:
            if dátum != "NI":
            
                adatok.append(
                    {
                        "dátum":dátum,
                        "dátum2": int("".join(dátum.split("."))),
                        "cím":cím,
                        "rész":rész,
                        "hossz":hossz,
                        "megtekintett":megtekintett
                    }
                )
            else:
                adatok.append(
                    {
                        "dátum":dátum,
                        "dátum2": int("".join(["0","0","0"])),
                        "cím":cím,
                        "rész":rész,
                        "hossz":hossz,
                        "megtekintett":megtekintett
                    }
                )
            index = 0
print("2. feladat")      
print(f"A listában {len([1 for x in adatok if x['dátum'] != 'NI'])} db vetítési dátummal rendelkező epizód van.\n")

print("3. feladat")
print(f"A listában lévő epizódok {round(len([1 for x in adatok if x['megtekintett'] == True])/len(adatok)*100,2)}%-át látta.\n")

print("4. feladat")
nézett_idő = sum([int(x["hossz"]) for x in adatok if x["megtekintett"] == True])
nap = nézett_idő // (24*60)
óra = nézett_idő % (24*60) // 60
perc =  nézett_idő % (24*60) % 60
print(f"Sorozatnézéssel {nap} napot {óra} órát és {perc} percet töltött.\n")

print("5. feladata")
dátum_in = int("".join(input("Adjon meg egy dátumot! Dátum= ").split(".")))
nem_látott = [[x["rész"],x["cím"]] for x in adatok if x["dátum"] != "NI" and x["dátum2"] <= dátum_in and not x["megtekintett"]]
for x in nem_látott:
    print(x[0],"\t",x[1])
print()

def Hetnapja(ev, ho, nap) -> str:
    napok = ("v", "h", "k", "sze","cs", "p", "szo")
    honapok = (0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4)
    if ho < 3:
        ev = ev -1
    return napok[(ev + ev // 4 - ev // 100 + ev // 400 + honapok[ho-1] + nap) % 7]
# print(Hetnapja(2020,5,35))
print("7. feladat")
nap_in = input("Adja meg a hét egy napját (például cs)! Nap= ")
aznapi = [x["cím"] for x in adatok if x["dátum"] != "NI" and Hetnapja(list(map(int, x["dátum"].split(".")))[0],list(map(int, x["dátum"].split(".")))[1],list(map(int, x["dátum"].split(".")))[2]) == nap_in]
if len(aznapi) == 0:
    print("Az adott napon nem került adásba sorozat")
else:
    for x in set(aznapi):
        print(x)

vetítési_idő = {x["cím"]:0 for x in adatok}
epizódok_száma = {x["cím"]:0 for x in adatok}

for x in adatok:
    vetítési_idő[x["cím"]] += x["hossz"]
    epizódok_száma[x["cím"]] += 1
# print(vetítési_idő["Games"], epizódok_száma["Games"])
with open("summa.txt","w") as kf:
    for x in vetítési_idő:
        kf.write(f"{x} {vetítési_idő[x]} {epizódok_száma[x]}\n")