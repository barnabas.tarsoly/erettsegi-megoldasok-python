nyers, adatok = [sor.strip() for sor in open("lista.txt", "r")], []
for sor in range(0, len(nyers), 5):
    if nyers[sor] != "NI": adatok.append({"dátum": nyers[sor],"dátum2": int("".join(nyers[sor].split("."))),"cím": nyers[sor+1],"rész": nyers[sor+2],"hossz": int(nyers[sor+3]),"megtekintett":bool(int(nyers[sor+4]))}) 
    else:adatok.append({"dátum": nyers[sor],"dátum2": int("".join(["0", "0", "0"])),"cím": nyers[sor+1],"rész": nyers[sor+2],"hossz": int(nyers[sor+3]),"megtekintett": bool(int(nyers[sor+4]))})
print(f'2. feladat\nA listában {len([1 for x in adatok if x["dátum"] != "NI"])} db vetítési dátummal rendelkező epizód van.\n3. feladat\nA listában lévő epizódok {round(len([1 for x in adatok if x["megtekintett"] == True])/len(adatok)*100,2)}%-át látta.\n4. feladat\nSorozatnézéssel {sum([int(x["hossz"]) for x in adatok if x["megtekintett"] == True]) // (24*60)} napot {sum([int(x["hossz"]) for x in adatok if x["megtekintett"] == True]) % (24*60) // 60} órát és {sum([int(x["hossz"]) for x in adatok if x["megtekintett"] == True]) % (24*60) % 60} percet töltött.\n5. feladat\n')
datum_in = input("Adjon meg egy dátumot! Dátum= ")
x = [[print(x["rész"],"\t",x["cím"])] for x in adatok if x["dátum"] != "NI" and x["dátum2"] <= int("".join(datum_in.split("."))) and not x["megtekintett"]]
def Hetnapja(ev, ho, nap) -> str:
    if ho < 3 :ev = ev - 1
    return ("v", "h", "k", "sze", "cs", "p", "szo")[(ev + ev // 4 - ev // 100 + ev // 400 + (0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4)[ho-1] + nap) % 7]
print("7. feladat")
napin = input("Adja meg a hét egy napját (például cs)! Nap= ")
aznapi = [print(y) for y in set([x["cím"] for x in adatok if x["dátum"] != "NI" and Hetnapja(list(map(int, x["dátum"].split(".")))[0], list(map(int, x["dátum"].split(".")))[1], list(map(int, x["dátum"].split(".")))[2]) == napin])]
if len(aznapi) == 0:print("Az adott napon nem került adásba sorozat")
vetítési_idő,epizódok_száma = {x["cím"]: 0 for x in adatok},{x["cím"]: 0 for x in adatok}
for x in adatok:
    vetítési_idő[x["cím"]] += x["hossz"]
    epizódok_száma[x["cím"]] += 1
with open("summa.txt", "w") as kf: 
    for x in vetítési_idő: kf.write(f"{x} {vetítési_idő[x]} {epizódok_száma[x]}\n")