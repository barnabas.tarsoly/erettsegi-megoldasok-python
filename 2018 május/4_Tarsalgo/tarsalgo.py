###69 perc szerkesztés

adatok = open("ajto.txt","r")

adatok_list = []

for i in adatok:
    i = i.strip().split()
    óra = int(i[0])
    perc = int(i[1])
    azonosító = int(i[2])
    irány = i[3]
    adatok_list.append({"óra":óra, "perc":perc, "azonosító":azonosító, "irány":irány})
adatok.close()    
print("2. feladat")

print(f'Az első belépő: {adatok_list[0]["azonosító"]}')

utolsó = 0

for k in adatok_list:
    if k["irány"] == "ki":
        utolsó = k["azonosító"]
        
print(f'Az utolsó kilépő: {utolsó}')

#3. feladat

azonosítók = []

for l in adatok_list:   
    if l["azonosító"] not in azonosítók:
        
        azonosítók.append(l["azonosító"])
        
azonosítók.sort()


áthaladás_file = open("athaladas.txt","w")
for i2 in azonosítók:
    darab = 0
    for i3 in adatok_list:
        if i3["azonosító"] == i2:
            darab += 1
    áthaladás_file.write(f"{i2} {darab}\n")
áthaladás_file.close()

print("\n4. feladat")

bentlévők = []
for k2 in adatok_list:
    
    if k2["azonosító"] in bentlévők:
        
        bentlévők.remove(k2["azonosító"])
    elif k2["irány"] == "be":
        bentlévők.append(k2["azonosító"])
        
bentlévők.sort()
print(bentlévők)
sor = ""
for l2 in bentlévők:
    sor = sor + f"{l2} "
print(f"A végén a társalgóban voltak: {sor}")

print("\n5. feladat")

bentlévők = []
idő_h = 0
idő_m = 0
darab = 0
max_bent = 0
for k3 in adatok_list:
    
    if k3["azonosító"] in bentlévők:
        
        bentlévők.remove(k3["azonosító"])
        darab -= 1
    elif k3["irány"] == "be":
        bentlévők.append(k3["azonosító"])
        darab += 1
    if darab >= max_bent:
        max_bent = darab
        idő_h = k3["óra"]
        idő_m = k3["perc"]

print(f"Például {idő_h}:{idő_m}-kor voltak a legtöbben a társalgóban")



print("\n6. feladat")

személyazonosító = int(input("Adja meg a személy azonosítóját! "))

print("\n7. feladat")

időpontok = []

for l3 in adatok_list:
    if l3["azonosító"] == személyazonosító:
        időpontok.append({"óra":l3["óra"], "perc":l3["perc"], "irány":l3["irány"]})
# print(időpontok)        
hetedik_szöveg = ""

for j in időpontok: 
    
    if j["irány"] == "be":
        if j["perc"] < 10:
        
            
            if j["óra"] < 10:
                hetedik_szöveg = hetedik_szöveg + f'0{j["óra"]}:0{j["perc"]}-'
            else:
                hetedik_szöveg = hetedik_szöveg + f'{j["óra"]}:0{j["perc"]}-'
        else:
            if j["óra"] < 10:
                hetedik_szöveg = hetedik_szöveg + f'0{j["óra"]}:{j["perc"]}-'
            else:
                hetedik_szöveg = hetedik_szöveg + f'{j["óra"]}:{j["perc"]}-'
    else:
        
        if j["perc"] < 10:
            if j["óra"] < 10:
            
                hetedik_szöveg = hetedik_szöveg + f'0{j["óra"]}:0{j["perc"]}\n'
            else:
                hetedik_szöveg = hetedik_szöveg + f'{j["óra"]}:0{j["perc"]}\n'
        else:
            if j["óra"] < 10:
                hetedik_szöveg = hetedik_szöveg + f'0{j["óra"]}:{j["perc"]}\n'
            else:
                hetedik_szöveg = hetedik_szöveg + f'{j["óra"]}:{j["perc"]}\n'

print(hetedik_szöveg)
    
print("\n8. feladat")
összesen_bent = 0
# be_idő = 0
idő_int_list_be = []
idő_int_list_ki = []
for h in időpontok:
    if h["irány"] == "be":
        számint = (h["óra"]-9)*60 + h["perc"]
        
        idő_int_list_be.append(számint)
    else:
        számint = (h["óra"]-9)*60 + h["perc"]
        
        idő_int_list_ki.append(számint)
# print(idő_int_list)
current = 0
bent = False
for h2 in idő_int_list_be:
    try:
        összesen_bent += idő_int_list_ki[current] - h2
    except:
        összesen_bent += 360-h2
        bent = True
    current += 1
if bent == True:
    print(f'A(z) {személyazonosító}. személy összesen {összesen_bent} percet volt bent, a megfigyelés végén a társalgóban volt.')
else:
    print(f'A(z) {személyazonosító}. személy összesen {összesen_bent} percet volt bent, a megfigyelés végén nem volt a társalgóban.')
