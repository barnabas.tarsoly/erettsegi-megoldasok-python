def f(x, text=""):
    print(f"\n{x}. feladat {text}")

f(1, "Adjon meg egy szót:")
str_in = input()
vowels = ["a","e", "i","o","u"]

if any(x in str_in for x in vowels):
    print("Van benne magánhangzó.")
else:
    print("Nincs benne magánhangzó.")

with open("szoveg.txt","r") as ff:
    max_len = 0
    text = ""
    for x in ff:
        x = x.strip()
        if len(x) > max_len:
            max_len = len(x)
            text = x
    f(2, f"Szó: {text}, hossz: {max_len}")

with open("szoveg.txt","r") as ff:
    szavak = []
    all_word = 0
    for x in ff:
        all_word += 1
        x = x.strip()
        vowel_n = 0
        for v in vowels:
            vowel_n += x.count(v)
        if vowel_n > len(x)-vowel_n:
            szavak.append(x)
    f(3, f"{len(szavak)}/{all_word} : {round((len(szavak)/all_word)*100,2)}%")

with open("szoveg.txt","r") as ff:
    szavak2 = []
    
    for x in ff:
        x = x.strip()
        if len(x) == 5:
            szavak2.append(x)
    
    f(4,"Szórészlet: ")
    részlet_in = input()
    létra = []
    for x in szavak2:
        
        if x[1:4] == részlet_in:
            létra.append(x)
    print(" ".join(létra))


with open("letra.txt","w")as kf:
    f(5)
    mid = list(set([x[1:4] for x in szavak2]))
    mid_dict = {}
    for x in mid:
        mid_dict[f"{x}"] = []
    for x in mid:
        for y in szavak2:
            if y[1:4] == x:
                mid_dict[x].append(y)
    for item in mid:
        if len(mid_dict[item]) >= 2:
            for x in mid_dict[item]:
                kf.write(f"{x}\n")
            kf.write("\n")
        

    