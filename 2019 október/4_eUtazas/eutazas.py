with open("utasadat.txt","r") as ff:
    adatok = []
    for sor in ff:
        sor = sor.strip().split()
        állomás = sor[0]
        dátum,idő = sor[1].split("-")
        azonosító = sor[2]
        típus = sor[3]
        érvényesség = sor[4]
        if int(érvényesség) == 0:
            érvényes = False
        elif int(dátum) > int(érvényesség) and int(érvényesség) > 20:
            érvényes = False
        else:
            érvényes = True
        adatok.append(
            {
                "állomás":állomás,
                "dátum":dátum,
                "idő":idő,
                "id":azonosító,
                "típus":típus,
                "érvényesség":érvényesség,
                "érvényes":érvényes

            }
        )

# print(adatok)
print("2. feladat")
print(f"A buszra {len(adatok)} utas akart felszállni.")

print("3. feladat")
érvénytelen = 0
for x in adatok:
    if not x["érvényes"]:
        # print(x)
        érvénytelen += 1

print(f"A buszra {érvénytelen} utas nem szállhatott fel.")

print("4. feladat")

állomások = {x["állomás"]:0 for x in adatok}
for x in adatok:
    állomások[x["állomás"]] += 1

print(f"A legtöbb utas ({állomások[max(állomások, key=lambda x: állomások[x])]} fő) a {max(állomások, key=lambda x: állomások[x])}. megállóban próbált felszállni.")

print("5. feladat")
ingyenes = 0
kedvezményes = 0

for x in adatok:
    if x["érvényes"]:
        if x["típus"] == "TAB" or x["típus"] == "NYB":
            # print(x)
            kedvezményes += 1
        elif x["típus"] == "NYP" or x["típus"] == "RVS" or x["típus"] == "GYK":
            ingyenes += 1

print(f"Ingyenes utazók száma: {ingyenes} fő")
print(f"A kedvezményesen utazók száma: {kedvezményes} fő")

def napokszama(e1:int, h1:int, n1:int, e2:int, h2:int, n2: int)-> int:
    h1 = (h1+9) % 12
    e1 = e1 - h1 // 10
    d1 = 365*e1+ e1 // 4 - e1 // 100 + e1 // 400 + (h1*306 + 5) // 10 + n1 -1
    h2 = (h2 +9 ) % 12
    e2 = e2 -h2 // 10
    d2 = 365*e2+e2 // 4 - e2 // 100 + e2 // 400 + (h2*306 + 5 ) // 10 + n2 -1
    return d2-d1

# print(napokszama(2021, 5, 10, 2021, 5, 13))
with open("figyelmeztetes.txt","w") as kf:
    for x in adatok:
        if x["érvényes"] and x["típus"] != "JGY":
            e1 = x["dátum"][0:4]
            h1 = x["dátum"][4:6]
            n1 = x["dátum"][6:8]
            # print(e1, h1, n1)
            e2  = x["érvényesség"][0:4]
            h2  = x["érvényesség"][4:6]
            n2  = x["érvényesség"][6:8]
            
            if napokszama(int(e1), int(h1), int(n1), int(e2),int( h2), int(n2)) <= 3:
                kf.write(f"{x['id']} {e2}-{h2}-{n2}\n")
