with open("tavirathu13.txt","r") as ff:
    adatok = []
    for sor in ff:
        sor = sor.strip().split()
        település = sor[0]
        idő, óra, perc = sor[1], sor[1][0:2], sor[1][2:4]
        szél, erő = sor[2][0:3], sor[2][3:5]
        hőm = sor[3]
        adatok.append({
            "város":település,
            "idő":idő,"óra":óra,"perc":perc,
            "szél":szél,"erő":erő,"szélirány":sor[2],
            "hőm":hőm
        })

print("2.feladat")
város_in = input("Adja meg egy település kódját! Település: ")
utolsó_adat = {}
for x in adatok:
    if x["város"] == város_in:
        utolsó_adat = x

print(f"Az utolsó mérési adat a megadott településről {utolsó_adat['óra']}:{utolsó_adat['perc']}-kor érkezett.")


print("3. feladat")

legnagyobb_hőm = -100
legnagyobb_hőm_adat = {}
legkisebb_hőm = 100
legkisebb_hőm_adat = {}
for x in adatok:
    if int(x["hőm"]) > legnagyobb_hőm:
        legnagyobb_hőm = int(x["hőm"])
        legnagyobb_hőm_adat = x
    if int(x["hőm"]) < legkisebb_hőm:
        legkisebb_hőm = int(x["hőm"])
        legkisebb_hőm_adat = x
print(f"A legalacsonyabb hőmérséklet {legkisebb_hőm_adat['város']} {legkisebb_hőm_adat['óra']}:{legkisebb_hőm_adat['perc']} {legkisebb_hőm_adat['hőm']} fok.")
print(f"A legmagasabb hőmérséklet {legnagyobb_hőm_adat['város']} {legnagyobb_hőm_adat['óra']}:{legnagyobb_hőm_adat['perc']} {legnagyobb_hőm_adat['hőm']} fok.")

print("4. feladat")

for x in adatok:
    if x["szélirány"] == "00000":
        print(f"{x['város']} {x['óra']}:{x['perc']}")

print("5. feladat")
városok_átlag = {x["város"]:[]for x in adatok}
városok_hők = {x["város"]:[]for x in adatok}

for város in városok_átlag.keys():
    óra = set()
    for adat in adatok:
        if adat["város"] == város:
            városok_hők[város].append(adat["hőm"])
            if adat["óra"] in ["01", "07", "13", "19"]:
                óra.add(adat["óra"])
                városok_átlag[város].append(adat["hőm"])
    if len(óra) < 4:
        városok_átlag[város] = "NA"

for város in városok_átlag.keys():
    if városok_átlag[város] == "NA":
        print(f"{város} NA; Hőmérséklet-ingadozás: {max(map(int,városok_hők[város]))-min(map(int,városok_hők[város]))}")
    else:
        print(f"{város} Középhőmérséklet: {round(sum(map(int, városok_átlag[város]))/len(városok_átlag[város]))}; Hőmérséklet-ingadozás: {max(map(int,városok_hők[város]))-min(map(int,városok_hők[város]))}")

print("6. feladat")
for város in set([x["város"] for x in adatok]):
    with open(f"{város}.txt","w") as kf:
        kf.write(f"{város}\n")
        for x in adatok:
            if x["város"] == város:
                kf.write(f"{x['óra']}:{x['perc']} {'#'*int(x['erő'])}\n")

print("A fájlok elkészültek.")