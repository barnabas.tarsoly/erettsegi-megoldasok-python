with open("autok.txt","r") as ff:
    adatok = []
    for sor in ff:
        sor = sor.strip().split()
        adatok.append(
            {
                "nap":int(sor[0]),
                "idő":sor[1],
                "rendszám":sor[2],
                "id":int(sor[3]),
                "km":int(sor[4]),
                "be":bool(int(sor[5])) #1 = be == True | 0 = ki == False
            }
        )
    # print(adatok)
print("2. feladat")
utolsó = {} 
for x in adatok:
    if x["be"] == False:
        utolsó = x
        # print(x)

print(f"{utolsó['nap']}. nap rendszám: {utolsó['rendszám']}")

print("3. feladat")
nap_in = int(input("Nap: "))
print(f"Forgalom a(z) {nap_in}. napon:")
for x in adatok:
    if x["nap"] == nap_in and x["be"] == False:
        print(f"{x['idő']} {x['rendszám']} {x['id']} ki")
    elif x["nap"] == nap_in and x["be"] == True:
        print(f"{x['idő']} {x['rendszám']} {x['id']} be")

print("4. feladat")

autók = {x["rendszám"]:True for x in adatok}

for x in adatok:
    autók[x['rendszám']] = x["be"]

nincs_bent = [x for x in autók if autók[x] == False]
print(f"A hónap végén {len(nincs_bent)} autót nem hoztak vissza.")
print("5. feladat")


autók_első_km = {x["rendszám"]:0 for x in adatok}
autók_utolsó_km = {x["rendszám"]:0 for x in adatok}
for x in set([y["rendszám"] for y in adatok]):
    for y in adatok:
        if y["rendszám"] == x:
            autók_első_km[x] = y["km"]
            break
for x in adatok:
    autók_utolsó_km[x["rendszám"]] = x["km"]

for x in autók_utolsó_km:
    print(f"{x} {autók_utolsó_km[x]-autók_első_km[x]} km")

print("6. feladat")
személyek = {x["id"]:0 for x in adatok}
for x in személyek:
    ki = 0
    
    for y in adatok:
        
        if x == y["id"] and y["be"] == False:
            
            ki = y["km"]
        elif x == y["id"] and y["be"] == True:
            # print(y)
            if személyek[x] < y["km"]-ki:
                személyek[x] = y["km"]-ki
                

print(f"A leghosszabb út: {személyek[max(személyek, key= lambda x: személyek[x])]} km, személy: {max(személyek, key= lambda x: személyek[x])}")

rendszám_in = input("Rendszám: ")
with open(f"{rendszám_in}.txt","w") as kf:
    kimenet = [[x["id"],x["nap"],x["idő"], x["km"]] for x in adatok if x["rendszám"] == rendszám_in and x["be"] == False]
    bemenet = [[x["id"],x["nap"],x["idő"], x["km"]] for x in adatok if x["rendszám"] == rendszám_in and x["be"] == True]
    for index, x in enumerate(kimenet):
        try:
            kf.write(f"{x[0]}\t{x[1]}. {x[2]}\t{x[3]} km\t{bemenet[index][1]}. {bemenet[index][2]}\t{bemenet[index][3]} km\n")
        except:
            kf.write(f"{x[0]}\t{x[1]}. {x[2]}\t{x[3]} km\n")
# print(kimenet)
# print(bemenet)
print("Menetlevél kész.")
