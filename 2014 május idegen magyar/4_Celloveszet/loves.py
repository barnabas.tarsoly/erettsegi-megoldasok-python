with open("verseny.txt","r",encoding="UTF-8") as ff:
    versenyzőID = 1
    elsőSor = True
    összesVersenyző = 0
    adatok = []
    for sor in ff:
        sor = sor.strip()
        if elsőSor:
            összesVersenyző = int(sor)
            elsőSor = False
        else:
            adatok.append({"id":versenyzőID,"lövések":sor})
            versenyzőID += 1

zsinorbanLőtt = []
for versenyző in adatok:
    zsinorban = 0
    for lövés in versenyző["lövések"]:
        
        if lövés == "+":
            zsinorban +=1
            
        elif lövés == "-":
            zsinorban = 0
        if zsinorban == 2:
            zsinorbanLőtt.append(versenyző["id"])
            break
print("2. feladat:")
print(f"Az egymást követően többször találó versenyzők: {' '.join(str(x) for x in zsinorbanLőtt)}")

for index, versenyző in enumerate(adatok):
    talált = 0
    for lövés in versenyző["lövések"]:
        if lövés == "+":
            talált += 1
    adatok[index]["talált"] = talált
találatok = [x["talált"] for x in adatok]
találatok.sort(reverse=True)
for versenyző in adatok:
    if versenyző["talált"] == találatok[0]:
        print("\n3. feladat:")
        print(f"A legtöbb lövést leadó versenyző rajtszáma: {versenyző['id']}")
        break

#feladat 4
def loertek(sor: str):
    aktpont = 20
    ertek = 0
    for i in range(0, len(sor)):
        
        if aktpont > 0 and sor[i] == "-":
            aktpont -= 1
        else:
            ertek += aktpont
    return ertek

print("\n5. feladat:")
versenyzőId = int(input("Adjon meg egy rajtszámot! "))
versenyző = adatok[versenyzőId-1]
hanyadik = []
for lövésSzáma, lövés in enumerate(versenyző["lövések"]):
    if lövés == "+":
        hanyadik.append(lövésSzáma+1)
print(f"5a. feladat: Célt érő lövések: {' '.join(str(x) for x in hanyadik)}")
print(f"5b. feladat: Az eltalált korongok száma: {versenyző['talált']}")


zsinorban = 0
lövések = 0
zsinor = []
for lövés in versenyző["lövések"]:
    if lövés == "+":
        zsinorban +=1
    elif lövés == "-" :
        zsinor.append(zsinorban)
        zsinorban = 0
zsinor.append(zsinorban)
zsinor.sort(reverse=True)
print(f"5c. feladat: A leghosszabb hibátlan sorozat hossza: {zsinor[0]}")

print(f"5d. feladat: A versenyző pontszáma: {loertek(versenyző['lövések'])}")

pontok = [] 
helyezés = 1
# dummy = 0
for index, versenyző in enumerate(adatok):
    adatok[index]["pont"] = loertek(versenyző["lövések"])
    if loertek(versenyző["lövések"]) not in pontok:
        pontok.append(loertek(versenyző["lövések"]))
pontok.sort(reverse=True)
with open("sorrend.txt","w",encoding="UTF-8") as kf:
    for pont in pontok:
        dummy = 0
        for versenyző in adatok:
            if versenyző["pont"] == pont:
                kf.write(f'{helyezés}\t{versenyző["id"]}\t{versenyző["pont"]}\n') 
                dummy += 1
        helyezés += dummy