táncok = ["cha-cha","salsa","rumba","samba","jive","tango","bachata"]
with open("tancrend.txt","r") as ff:
    adatok = []
    névTemp = []
    darab = 0
    for sor in ff:
        sor = sor.strip()

        if sor in táncok:
            tánc = sor
            
        else:
            névTemp.append(sor)
            darab += 1
            if darab == 2:
                adatok.append({"tánc":tánc,"pár":névTemp})
                névTemp = []
                darab = 0
print("2. feladat:")
print(f'Elsőként bemutatott tánc neve: {adatok[0]["tánc"]}\nUtolsóként bemutatott tánc: {adatok[-1]["tánc"]}.')

print("\n3. feladat:")
print(f'{len([x["pár"] for x in adatok if x["tánc"] == "samba" ])} pár mutatta be a sambát.')
print("\n4. feladat:")
print(f'Vilma a következő táncokban szerepelt: {", ".join([x["tánc"] for x in adatok if "Vilma" in x["pár"]])}')
print("\n5. feladat:")
táncIn = input("A tánc neve: ")
párja = ""
talált = False
for adat in adatok:
    if "Vilma" in adat["pár"] and adat["tánc"] == táncIn:
        talált = True
        for x in adat["pár"]:
            if x != "Vilma":
                print(f'A {táncIn} bemutatóján Vilma párja {x} volt.')
if not talált:
    print(f"Vilma nem táncolt {táncIn}-t.")
fiúk = list(dict.fromkeys([x["pár"][1] for x in adatok]))
lányok = list(dict.fromkeys([x["pár"][0] for x in adatok]))
with open("szereplok.txt","w", encoding="UTF-8") as kf:
    kf.write(f'Lányok: {", ".join(lányok)}\n')
    kf.write(f'Fiúk: {", ".join(fiúk)}')

nevekTemp = []
nevekÖsszes = []
for x in adatok:
    for név in x["pár"]:
        nevekTemp.append(név)
        if név not in nevekÖsszes:
            nevekÖsszes.append(név)
legnagyobb = 0
névLegnagyobb = []
def legnagyobb(lista, lista2):
    legnagyobb = 0
    névLegnagyobb = []
    for x in lista2:
        if lista.count(x) > legnagyobb:
            legnagyobb = nevekTemp.count(x)
            névLegnagyobb = [x]
        elif lista.count(x) == legnagyobb:
            névLegnagyobb.append(x)
    return névLegnagyobb


fiúkLegnagyobb = legnagyobb(nevekÖsszes, fiúk)
lányokLegnagyobb = legnagyobb(nevekÖsszes, lányok)
print("\n7. feladat:")    
print(f'Legtöbbször a fúk közül: {", ".join(fiúkLegnagyobb)}')
print(f'Legtöbbször a lányok közül: {", ".join(lányokLegnagyobb)}')


